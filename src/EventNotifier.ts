import {Subject} from "rxjs/Subject";
import {Subscription} from "rxjs/Subscription";

interface IEventNotifier{
    on(eventName:string, callback:Function );
    trigger( eventName:string, data:any );
}

class EventNotifier implements IEventNotifier {
    protected _events:{ [eventName: string]: Subject<any>; } ;

    constructor( events?:string[] ){
        this._events = {};
        if(events){
            for( var i in events ){
                this.createEvent(events[i]);
            }
        }
    }

    public createEvent( eventName:string ){
        if(!this._events[eventName]){
            this._events[eventName] = new Subject<any>();
        }
    }

    public on( eventName:string, callback:any ):Subscription{
        return this._events[eventName].subscribe( callback );
    }

    public trigger( eventName:string, data:any=[] ){
        return this._events[eventName].next( data );
    }

    public triggerError( eventName:string, data:any=[] ){
        return this._events[eventName].error( data );
    }
}

export { IEventNotifier, EventNotifier };
