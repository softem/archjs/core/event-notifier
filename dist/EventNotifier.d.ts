import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
interface IEventNotifier {
    on(eventName: string, callback: Function): any;
    trigger(eventName: string, data: any): any;
}
declare class EventNotifier implements IEventNotifier {
    protected _events: {
        [eventName: string]: Subject<any>;
    };
    constructor(events?: string[]);
    createEvent(eventName: string): void;
    on(eventName: string, callback: any): Subscription;
    trigger(eventName: string, data?: any): void;
    triggerError(eventName: string, data?: any): void;
}
export { IEventNotifier, EventNotifier };
