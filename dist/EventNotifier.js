"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Subject_1 = require("rxjs/Subject");
class EventNotifier {
    constructor(events) {
        this._events = {};
        if (events) {
            for (var i in events) {
                this.createEvent(events[i]);
            }
        }
    }
    createEvent(eventName) {
        if (!this._events[eventName]) {
            this._events[eventName] = new Subject_1.Subject();
        }
    }
    on(eventName, callback) {
        return this._events[eventName].subscribe(callback);
    }
    trigger(eventName, data = []) {
        return this._events[eventName].next(data);
    }
    triggerError(eventName, data = []) {
        return this._events[eventName].error(data);
    }
}
exports.EventNotifier = EventNotifier;
//# sourceMappingURL=EventNotifier.js.map