# event-enotifier 
## Info
**language:** typescript

**module type:** commonjs

**dependncies:** rxjs

**description:**
Event emitter/notifier class using rxjs/Subject as events. 

## Instalation 

`npm install @archjs/event-notifier`

package.json

```javascript 
{
   ...
   dependencies:{
      ...
      "@archjs/event-notifier": "*",
      ...
   }
   ...
}
```

development install
``git clone git@gitlab.com:softem/archjs/core/event-notifier.git``

## Usage
### creation

```typescript
import {EventNotifier} from '@archjs/event-notifier/EventNotifier';
var norifier = new EventNotifier(); // no cosntructor parameters
var norifier = new EventNotifier(['click','change']); //constructor with events
```

## Register new event to notifier
```typescript
notifier.createEvent('myCustomEventName');
```
## Attach new observer to event
```typescript
notifier.on('myCustomEventName', function(data){ 
    console.log('myCustomEventName triggered');
});
```

## Trigger event
```typescript
notifier.trigger('myCustomEventName', { name:'myCustomEventName' } );
```

## Unsubscribe
```typescript
var observer = notifier.on('myCustomEventName', function(data){ 
    console.log('myCustomEventName triggered');
});
observer.unsubscribe();
```
